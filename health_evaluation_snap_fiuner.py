# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2015 Cédric Krier
#    Copyright (C) 2014-2015 Chris Zimmerman <siv@riseup.net>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date


from trytond.pool import Pool, PoolMeta
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval, Or, Equal

__all__ = ['PatientEvaluation']


class PatientEvaluation(metaclass=PoolMeta):
    'Patient Evaluation'
    __name__ = 'gnuhealth.patient.evaluation'
    
    STATES = {'readonly': Eval('state') == 'signed'}
    
    related_evaluation = fields.Many2One('gnuhealth.patient.evaluation',
                                         'Related Evaluation',
                                         readonly= True)

       
    @classmethod
    @ModelView.button
    def evaluation_snap(cls, evaluations):
        for evaluation in evaluations:
            related = cls.copy([evaluation],{
                    'code': None,
                    'related_evaluation': evaluation.id,
                    'evaluation_start': evaluation.default_evaluation_start(),
                    'evaluation_endtime': None
                    })
            cls.write(related,{
                    'code': related[0].code + " RELATED(" + evaluation.code + ") "    
                    })

    @classmethod
    def __setup__(cls):
        super(PatientEvaluation, cls).__setup__()
        
        cls._buttons.update({
            'evaluation_snap': {'invisible': Eval('state').in_(['in_progress','done',])}
            })
        
        cls.code.readonly = True
